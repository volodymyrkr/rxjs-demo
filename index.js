console.log(rxjs);

var stream$ = rxjs.Observable.create(function(observer) {
    observer.next('One');
    setInterval(function() {
        observer.next(Date.now());
    }, 1000);
    setTimeout(function() {
        observer.error("Some Error");
    }, 1000);
    setTimeout(function() {
        observer.complete();
    }, 3000)
});

stream$
    .subscribe(
        function(data) {
            console.log(data);
        },
        function(error) {
            console.log(error);
        },
        function () {
            console.log("Stream is Completed");
        }
    );
